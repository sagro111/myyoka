import React, { useEffect } from 'react';
import { observer } from 'mobx-react';
import { useStore } from '../../context';
import './calendar.scss';
import { RangeCalendar } from '../../components/rangeCalendar/RangeCalendar';
import { CalendarPageFilters } from './CalendarPageFilters';
import { ContentWrapper } from '../../components/contentWrapper/ContentWrapper';
import { LoaderWrapper } from '../../components/loaderWrapper/LoaderWrapper';

export const CalendarPage = observer(() => {
    const { calendarStore } = useStore();
    const { nighClub, concert, party } = calendarStore;

    useEffect(() => {
        // calendarStore.fetchCollection();
    }, [calendarStore]);

    return (
        <div className="calendar-page">
            <div className="calender-page__aside">
                <RangeCalendar onSelectDate={() => {}} />

                <CalendarPageFilters onSubmit={() => {}} />
            </div>

            <div className="calender-page__content">
                <LoaderWrapper isLoading={calendarStore.isLoading}>
                    <>
                        {party && <ContentWrapper title="Party" items={party} link="party" />}
                        {concert && (
                            <ContentWrapper title="Concert" items={concert} link="concert" />
                        )}
                        {nighClub && (
                            <ContentWrapper title="Night club" items={nighClub} link="nigth_club" />
                        )}
                    </>
                </LoaderWrapper>
            </div>
        </div>
    );
});
