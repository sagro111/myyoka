import React from 'react';
import { Field, Form, FormRenderProps } from 'react-final-form';
import { FormButtons } from '../../components/formButtons/FormButtons';
import { FinalSelect } from '../../components/finalForm/FinalSelect';
import { FinalMultipleCheckbox } from '../../components/finalForm/FinalMultipleCheckbox';

type CalendarPageFiltersProps = {
    onSubmit: (values: any) => void;
};

const eventTypeOptions = [
    { label: 'Party', id: 'party' },
    { label: 'Concert', id: 'concert' },
    { label: 'Night club', id: 'night_club' },
];

const featureOptions = [
    { label: 'Free enter', id: 'free_enter' },
    { label: 'With friends', id: 'with_friend' },
    { label: 'Near me', id: 'near_me' },
    { label: 'Night Club', id: 'night_club' },
    { label: 'Night Club', id: 'night_club' },
];

export const CalendarPageFilters = ({ onSubmit }: CalendarPageFiltersProps) => (
    <div className="calender-page-filters">
        <span className="calender-page-filters__title">Filters</span>

        <Form onSubmit={onSubmit}>
            {({ handleSubmit, values, form }: FormRenderProps) => (
                <form autoComplete="off" onSubmit={handleSubmit}>
                    <Field
                        name="eventType"
                        label="Event type"
                        options={eventTypeOptions}
                        component={FinalSelect}
                    />

                    {values.eventType?.id === 'party' && (
                        <Field
                            name="feature"
                            options={featureOptions}
                            label="Options"
                            component={FinalMultipleCheckbox}
                        />
                    )}

                    <Field
                        name="genre"
                        label="Genre"
                        component={FinalSelect}
                        options={eventTypeOptions}
                    />
                    {values.eventType === 'party' && 'party'}
                    <FormButtons onReset={form.reset} onSubmit={form.submit} />
                </form>
            )}
        </Form>
    </div>
);
