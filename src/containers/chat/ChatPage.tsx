import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import './ChatPage.scss';
import { useStore } from '../../context';
import { Tabs } from '../../components/ui/tabs/Tabs';
import { ChatChanelTypeProps } from '../../store/mobx/ChatStore';
import { ChatPageChatsList } from './ChatPageChatsList';
import { ChatPageMessageList } from './ChatPageMessageList';

export const ChatPage = observer(() => {
    const { createChatStore } = useStore();
    const [chatStore] = useState(() => createChatStore());

    useEffect(() => {
        chatStore.fetchChats(chatStore.chanelType);
    }, [chatStore.getChanelType]);

    return (
        <div className="chat-page">
            <div>
                <Tabs
                    tabs={[
                        {
                            label: 'Компании',
                            type: 'companies',
                        },
                        {
                            label: 'Контакты',
                            type: 'contacts',
                        },
                    ]}
                    onClick={(tabsType: ChatChanelTypeProps) => chatStore.setChanelType(tabsType)}
                    currentTab={chatStore.getChanelType}
                />
                <ChatPageChatsList
                    chatStore={chatStore}
                    isLoading={chatStore.isLoading}
                    chatsList={chatStore.chatList}
                />
            </div>

            <div>{chatStore.activeChat && <ChatPageMessageList chatStore={chatStore} />}</div>
        </div>
    );
});

export default ChatPage;
