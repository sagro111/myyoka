import React from 'react';
import { Field, Form } from 'react-final-form';
import ChatStore from '../../store/mobx/ChatStore';

type ChatPageMessageListProps = {
    chatStore: ChatStore;
};

export const ChatPageMessageList = ({ chatStore }: ChatPageMessageListProps) => (
    <div className="chat">
        <Form onSubmit={chatStore.sendMessage}>
            {({ handleSubmit }) => (
                <form onSubmit={handleSubmit}>
                    <Field name="message" component="input" />
                    <button type="button"> Send</button>
                </form>
            )}
        </Form>
    </div>
);
