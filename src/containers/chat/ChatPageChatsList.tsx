import React from 'react';
import { observer } from 'mobx-react';
import cn from 'classnames';
import Icon from '@mdi/react';
import { mdiChevronRight } from '@mdi/js';
import { Loader } from '../../components/loader';
import { Image } from '../../components/ui/image';
import ChatStore, { ChatListProps } from '../../store/mobx/ChatStore';

type ChatPageChatsListProps = {
    chatsList: Array<ChatListProps>;
    isLoading: boolean;
    chatStore: ChatStore;
};

export const ChatPageChatsList = observer(
    ({ chatsList, isLoading, chatStore }: ChatPageChatsListProps) => (
        <div className="chat-list">
            {isLoading ? (
                <Loader />
            ) : (
                chatsList.map((item) => {
                    const chatItemClass = cn({
                        'chat-list-item': true,
                        active: chatStore.activeChat?.id === item.id,
                    });
                    return (
                        <div
                            onClick={() => chatStore.setActiveChat(item)}
                            className={chatItemClass}
                            key={item.id}
                        >
                            <div className="chat-list-item__img">
                                <Image src={item.avatar} alt="chat avatar" />
                            </div>
                            <span className="chat-list-item__name">
                                {item.name} {item.surname}
                            </span>

                            <div className="chat-list-item__message">
                                <p>{item.lastMessage}</p>
                            </div>

                            <p className="chat-list-item__date">{item.date}</p>

                            <span className="chat-list-item__chevron">
                                <Icon path={mdiChevronRight} size={1} />
                            </span>
                        </div>
                    );
                })
            )}
        </div>
    )
);
