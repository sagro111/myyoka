import React from 'react';
import './styles/baseLayout.scss';
import { observer } from 'mobx-react';
import Sidebar from './Sidebar';
import { Router } from '../../Routers';
import { useStore } from '../../context';
import { UserCabinet } from '../../components/userCabinet/UserCabinet';
import { Popover } from '../../components/popover/Popover';
import { SignInForm } from './SignInForm';
import { SignUpForm } from './SignUpForm';
import { Loader } from '../../components/loader';
import { CongratulationsBlock } from '../../components/congratulationsBlock/CongratulationsBlock';

export const BaseLayout = observer(() => {
    const { authStore } = useStore();

    return (
        <div className="base-layout">
            <Sidebar />
            <UserCabinet
                logout={authStore.logout}
                user={authStore.user}
                onClick={authStore.changeContext}
            />
            <Router />

            {authStore.context === 'sign_up_form' && (
                <Popover size="sm" onClose={() => authStore.changeContext(null)}>
                    <div className="base-layout__forms">
                        {authStore.isLoading && <Loader />}
                        {authStore.congratsEmail ? (
                            <CongratulationsBlock
                                close={() => {
                                    authStore.changeContext(null);
                                    authStore.setCongratsEmail(null);
                                }}
                                email={authStore.congratsEmail}
                            />
                        ) : (
                            <>
                                <SignInForm onSubmit={authStore.signIn} />
                                <p className="or">or</p>
                                <SignUpForm onSubmit={authStore.signUp} />
                            </>
                        )}
                    </div>
                </Popover>
            )}
        </div>
    );
});
