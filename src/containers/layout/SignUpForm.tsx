import React from 'react';
import { Field, Form, FormRenderProps } from 'react-final-form';
import { FinalInput } from '../../components/finalForm/FinalInput';
import { FinalInputPassword } from '../../components/finalForm/FinalPasswordInput';
import { Button } from '../../components/ui/button/Button';
import { FinalCheckbox } from '../../components/finalForm/FinalCheckbox';

type SignUpFormProps = {
    onSubmit: (values: any) => void;
};

export const SignUpForm = ({ onSubmit }: SignUpFormProps) => (
    <Form
        onSubmit={onSubmit}
        initialValues={{
            email: 'sagromagro@gmail.com',
            passwordConfirmation: 'Sol12345',
            terms: true,
        }}
    >
        {({ handleSubmit }: FormRenderProps) => (
            <form onSubmit={handleSubmit}>
                <p className="form-title">You can also join us</p>
                <div className="two-columns">
                    <Field component={FinalInput} label="First name" name="firstName" />
                    <Field component={FinalInput} label="Last name" name="lastName" />
                </div>

                <Field component={FinalInput} label="E-mail" name="email" />

                <Field
                    component={FinalInputPassword}
                    label="Password"
                    name="password"
                    type="password"
                />

                <Field
                    component={FinalInputPassword}
                    label="Confirm password"
                    name="passwordConfirmation"
                    type="password"
                />

                <div className="buttons-block">
                    <Field
                        name="terms"
                        label="- You accept our Terms and Conditions"
                        component={FinalCheckbox}
                    />

                    <Button label="Submit" view="secondary" />
                </div>
            </form>
        )}
    </Form>
);
