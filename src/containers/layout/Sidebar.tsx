import React from 'react';
import './styles/sidebar.scss';
import { observer } from 'mobx-react';
import Logo from '../../components/logo/Logo';
import { Menu } from '../../components/menu/Menu';
import {
    mbBellIcon,
    mbCalendarIcon,
    mbChatIcon,
    mbEventsIcon,
    mbHomeIcon,
    mbMapsIcon,
    mbPlus,
    mbShareIcon,
} from '../../components/ui/icon/Icons';
import { PrimaryIcon } from '../../components/ui/icon';
import { useStore } from '../../context';

const Sidebar = observer(() => {
    const { authStore } = useStore();

    return (
        <aside className="sidebar">
            <Logo size="full" />

            <Menu
                menuItems={[
                    {
                        icon: mbPlus,
                        link: '/create_event',
                        accessible: authStore.isAuthed,
                    },
                    {
                        icon: mbHomeIcon,
                        link: '/',
                        accessible: true,
                    },
                    {
                        icon: mbMapsIcon,
                        link: '/maps',
                        accessible: true,
                    },
                    {
                        icon: mbCalendarIcon,
                        link: '/calendar',
                        accessible: true,
                    },
                    {
                        icon: mbShareIcon,
                        link: '/media',
                        accessible: true,
                    },
                    {
                        icon: mbEventsIcon,
                        link: '/events',
                        accessible: true,
                    },
                    {
                        icon: mbChatIcon,
                        link: '/chat',
                        accessible: authStore.isAuthed,
                    },
                ]}
            />

            <div className="settings-menu">
                <span className="bell-ico">
                    <PrimaryIcon icon={mbBellIcon} />
                </span>
            </div>
        </aside>
    );
});

export default Sidebar;
