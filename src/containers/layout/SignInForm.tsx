import React from 'react';
import { Field, Form, FormRenderProps } from 'react-final-form';
import { FinalInput } from '../../components/finalForm/FinalInput';
import { FinalInputPassword } from '../../components/finalForm/FinalPasswordInput';
import { FinalCheckbox } from '../../components/finalForm/FinalCheckbox';
import { Button } from '../../components/ui/button/Button';

type SignInFormProps = {
    onSubmit: (values: any) => void;
};

export const SignInForm = ({ onSubmit }: SignInFormProps) => (
    <Form onSubmit={onSubmit}>
        {({ handleSubmit }: FormRenderProps) => (
            <form onSubmit={handleSubmit}>
                <p className="form-title">If you are already registered</p>
                <Field component={FinalInput} label="E-mail" name="email" />
                <Field component={FinalInputPassword} label="Password" name="password" />

                <div className="buttons-block">
                    <Field name="notMyPC" label="Other people PC" component={FinalCheckbox} />

                    <Button label="Sign In" view="secondary" />
                </div>
            </form>
        )}
    </Form>
);
