import React, { useState } from 'react';
import { observer } from 'mobx-react';
import { Form, FormRenderProps } from 'react-final-form';
import { useStore } from '../../context';
import './styles/SettingsPage.scss';
import { LoaderWrapper } from '../../components/loaderWrapper/LoaderWrapper';
import { PersonalInfoForm } from './PersonalInfoForm';
import { SettingsProgress } from '../../components/settingsProgress/SettingsProgress';

export const SettingsPage = observer(() => {
    const { createSettingsStore } = useStore();
    const [settingsStore] = useState(() => createSettingsStore());
    return (
        <div className="settings-page">
            <div className="settings-page__navigation">
                <SettingsProgress percent={75} />
            </div>
            <div className="settings-page__form">
                <LoaderWrapper isLoading={settingsStore.isLoading}>
                    <Form initialValues={settingsStore.user} onSubmit={settingsStore.updateProfile}>
                        {({ handleSubmit }: FormRenderProps) => (
                            <form onSubmit={handleSubmit}>
                                <PersonalInfoForm />

                                <button type="button">Submit</button>
                            </form>
                        )}
                    </Form>
                </LoaderWrapper>
            </div>
        </div>
    );
});
