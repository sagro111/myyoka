import React from 'react';
import { Field } from 'react-final-form';
import { FinalInput } from '../../components/finalForm/FinalInput';
import { FlexRow } from '../../components/flexRow/FlexRow';
import { FinalPhotoUploader } from '../../components/finalForm/FinalPhotoUploader';
import { FinalDatepicker } from '../../components/finalForm/FinalDatepicker';
import { FinalTelInput } from '../../components/finalForm/FinalTelInput';

export const PersonalInfoForm = () => (
    <>
        <div className="main-info">
            <div className="main-info__personal">
                <p className="main-info__title">Personal information</p>
                <p className="main-info__desc">Lorem ipsum dolar sit amet</p>

                <Field component={FinalInput} label="E-mail" name="email" />

                <FlexRow columns={2}>
                    <Field component={FinalInput} label="First name" name="firstName" />
                    <Field component={FinalInput} label="Last name" name="lastName" />
                </FlexRow>
            </div>

            <div className="main-info__avatar">
                <Field component={FinalPhotoUploader} name="avatar" />
            </div>
        </div>

        <FlexRow columns={3}>
            <Field component={FinalInput} label="Gender" name="gender" />
            <Field component={FinalTelInput} label="Telephone" name="tel" />
            <Field component={FinalDatepicker} label="Birthday" name="birthday" />
        </FlexRow>

        <FlexRow columns={2}>
            <Field component={FinalInput} label="City" name="city" />
            <Field component={FinalInput} label="Country" name="country" />
        </FlexRow>
    </>
);
