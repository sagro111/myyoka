import React from 'react';
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from 'react-google-maps';

const demoFancyMapStyles = require('./MapsStyle.json');

const MapWithAMarker = withScriptjs(
    withGoogleMap(() => (
        <GoogleMap
            defaultZoom={8}
            defaultCenter={{ lat: -34.397, lng: 150.644 }}
            defaultOptions={{ styles: demoFancyMapStyles }}
        >
            <Marker position={{ lat: -34.397, lng: 150.644 }} />
            <Marker position={{ lat: -34.347, lng: 150.624 }} />
            <Marker position={{ lat: -34.337, lng: 150.634 }} />
            <Marker position={{ lat: -34.317, lng: 150.644 }} />
        </GoogleMap>
    ))
);

export const MapsPage = () => (
    <div>
        <MapWithAMarker
            // @ts-ignore
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjzK2v46G1iafmL9ss7FI5WwK_UmayDRE&v=3.exp&libraries=geometry,drawing,places"
            // @ts-ignore
            loadingElement={<div style={{ height: `110%` }} />}
            // @ts-ignore
            containerElement={
                <div style={{ height: `100%`, width: '105%', transform: 'translateX(-4%)' }} />
            }
            // @ts-ignore
            mapElement={<div style={{ height: `100%` }} />}
        />
    </div>
);
