import React from 'react';
import './app.scss';
import { BaseLayout } from '../layout/BaseLayout';
import { Notification } from '../Notification/Notification';

const App = () => (
    <>
        <Notification />
        <BaseLayout />
    </>
);

export default App;
