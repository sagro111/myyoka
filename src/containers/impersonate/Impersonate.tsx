import React, { useEffect } from 'react';
import { observer } from 'mobx-react';
import { useStore } from '../../context';
import { useQuery } from '../../utils/useQuery';
import { parseJwt } from '../../utils/parseJwt';
import { StatusEnum } from '../../models/User';

export const Impersonate = observer(() => {
    const { authStore } = useStore();
    const token = useQuery('token');
    const userData = parseJwt(token);

    useEffect(() => {
        if (token && userData.status === StatusEnum.pending) {
            authStore.confirm(token);
        }
    }, []);
    return <div />;
});

export default Impersonate;
