import React from 'react';
import { observer } from 'mobx-react';
import { useStore } from '../../context';
import { bem } from '../../utils/bem';

const cn = bem('notification');
export const Notification = observer(() => {
    const { notificationStore } = useStore();
    const { getNotification, notificationItem } = notificationStore;
    return (
        <div
            className={cn()}
            onClick={() => notificationStore.closeNotification()}
            onAbortCapture={() => console.log()}
            onChange={() => {
                console.log(1);
            }}
        >
            {getNotification && <p>{notificationItem.message}</p>}
        </div>
    );
});
