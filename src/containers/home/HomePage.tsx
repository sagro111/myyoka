import React, { useEffect } from 'react';
import './styles/homePage.scss';
import { observer } from 'mobx-react';
import { useStore } from '../../context';

export const HomePage = observer(() => {
    const { homeStore } = useStore();

    useEffect(() => {
        homeStore.fetchEventsCollection();
    }, [homeStore]);

    return (
        <div className="home-page">
            {/* <SoonEventsWidget items={homeStore.soonEventsData}/> */}
        </div>
    );
});
