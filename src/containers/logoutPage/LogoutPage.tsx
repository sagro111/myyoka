import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import { useStore } from '../../context';

export const LogoutPage = () => {
    const { authStore } = useStore();
    const history = useHistory();

    useEffect(() => {
        authStore.logout().then(() => history.push('/'));
    }, []);
    return <div />;
};
