import React from 'react';
import { Field } from 'react-final-form';
import { FlexRow } from '../../components/flexRow/FlexRow';
import { FinalSelect } from '../../components/finalForm/FinalSelect';
import { FieldWrapper } from '../../components/fieldWrapper/FieldWrapper';
import { FinalInput } from '../../components/finalForm/FinalInput';

export const CreateEventMainInfo = () => (
    <div>
        <h2>Main info</h2>
        <FlexRow columns={2}>
            <div>
                <FlexRow columns={2}>
                    <FieldWrapper label="Party name">
                        <Field name="name" placeholder="Enter name" component={FinalInput} />
                    </FieldWrapper>
                    <FieldWrapper label="Country">
                        <Field name="country" placeholder="USA" component={FinalInput} />
                    </FieldWrapper>
                </FlexRow>
                <FlexRow columns={1}>
                    <FieldWrapper label="Gender">
                        <Field name="name" placeholder="Select gender" component={FinalSelect} />
                    </FieldWrapper>
                </FlexRow>
                <FlexRow columns={3}>
                    <FieldWrapper label="Min age">
                        <Field name="minAge" placeholder="16" numeric component={FinalInput} />
                    </FieldWrapper>
                    <FieldWrapper label="Max peoples count">
                        <Field name="maxPeople" placeholder="100" numeric component={FinalInput} />
                    </FieldWrapper>
                    <FieldWrapper label="Enter price">
                        <Field name="price" placeholder="10$" component={FinalInput} />
                    </FieldWrapper>
                </FlexRow>
            </div>
        </FlexRow>
    </div>
);
