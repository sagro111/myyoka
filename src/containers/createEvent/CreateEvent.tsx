import React, { useState } from 'react';
import { Form } from 'react-final-form';
import './styles/CreateEvent.scss';
import { observer } from 'mobx-react';
import { useHistory, useLocation } from 'react-router';
import { CreateEventSelectType } from './CreateEventSelectType';
import { useStore } from '../../context';
import { CreateEventMainInfo } from './CreateEventMainInfo';
import { CreateEventTimePlacement } from './CreateEventTimePlacement';
import { CreateEventMedia } from './CreateEventMedia';
import { CreateEventStepperItem } from '../../models/CreateEvent';
import { CreateEventStepper } from '../../components/createEventStepper/CreateEventStepper';
import { bem } from '../../utils/bem';
import { Button } from '../../components/ui/button/Button';
import { getSessionStorageData, saveDataToStorage } from '../../utils/sessionStorage';

const cn = bem('create-event');
export const CreateEvent = observer(() => {
    const activeStep: CreateEventStepperItem | unknown = useLocation().state || 'select_type';
    const history = useHistory();
    const { createEventStore } = useStore();
    const [eventStore] = useState(createEventStore);

    const steps = (formSubmit?: any): Array<CreateEventStepperItem> => [
        {
            activeStep: 'select_type',
            component: <CreateEventSelectType onClick={formSubmit} />,
            label: 'Select type',
        },
        {
            activeStep: 'main_info',
            component: <CreateEventMainInfo />,
            label: 'Main info',
        },
        {
            activeStep: 'time_placement',
            component: <CreateEventTimePlacement />,
            label: 'Time and Placement',
        },
        {
            activeStep: 'media',
            component: <CreateEventMedia />,
            label: 'Media',
        },
    ];

    return (
        <div className={cn()}>
            <Form
                initialValues={getSessionStorageData('create_event') || {}}
                onSubmit={eventStore.submitForm}
            >
                {({ handleSubmit, form, values }) => {
                    saveDataToStorage('create_event', values);
                    return (
                        <form onSubmit={handleSubmit}>
                            <div
                                className={cn('stepper', { active: activeStep !== 'select_type' })}
                            >
                                <CreateEventStepper
                                    withSingleton
                                    activeStep={activeStep}
                                    tabs={steps()}
                                />

                                {activeStep !== 'select_type' && (
                                    <>
                                        {activeStep !== 'main_info' && (
                                            <Button
                                                isSquare
                                                view="primary"
                                                onClick={() => history.goBack()}
                                                label="Back"
                                                type="button"
                                            />
                                        )}
                                        <Button
                                            isSquare
                                            view="secondary"
                                            label={activeStep === 'media' ? 'Create' : 'Next'}
                                            type="submit"
                                        />
                                    </>
                                )}
                            </div>
                            {steps(form.submit).map(
                                (item) =>
                                    item.activeStep === activeStep && (
                                        <React.Fragment key={item.activeStep}>
                                            {item.component}
                                        </React.Fragment>
                                    )
                            )}
                        </form>
                    );
                }}
            </Form>
        </div>
    );
});
