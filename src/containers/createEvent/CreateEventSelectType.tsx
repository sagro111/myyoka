import React from 'react';
import { Field } from 'react-final-form';
import './styles/CreateEvent.scss';
import { SelectEventTypeAdapter } from '../../components/selectEventTypeAdapter/SelectEventTypeAdapter';

type CreateEventSelectTypeProps = {
    onClick: () => void;
};

export const CreateEventSelectType = ({ onClick }: CreateEventSelectTypeProps) => (
    <div className="create-event__select-type">
        <p className="create-event__info">Hi, I will help you create an event</p>
        <p className="create-event__about-step">The first step is to choose the type of event</p>

        <Field name="eventType" onClick={onClick} component={SelectEventTypeAdapter} />
    </div>
);
