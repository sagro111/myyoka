import React from 'react';
import './styles/ContentWrapper.scss';
import { Link } from 'react-router-dom';
import Icon from '@mdi/react';
import { mdiChevronRight } from '@mdi/js';
import EventCard from '../eventCard/EventCard';

type ContentWrapperProps = {
    title: string;
    link: string;
    items: Array<any>;
};

export const ContentWrapper = ({ title, link, items = [] }: ContentWrapperProps) => (
    <div className="content-wrapper">
        <h3 className="content-wrapper__title">{title}</h3>
        <div className="content-wrapper__content">
            {items.map((item) => (
                <EventCard key={item.link} {...item} />
            ))}
        </div>

        <Link className="content-wrapper__link" to={link}>
            {' '}
            Показать ещё <Icon size={1} path={mdiChevronRight} />
        </Link>
    </div>
);
