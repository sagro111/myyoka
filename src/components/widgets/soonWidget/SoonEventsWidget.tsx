import React from 'react';
import './styles/soonWidget.scss';
import { Link } from 'react-router-dom';
import { EventsCommonTypes } from '../../../models';

type SoonEventsWidgetItemProps = {
    item: EventsCommonTypes;
};

type SoonEventsWidgetProps = {
    items: Array<EventsCommonTypes>;
};

const SoonEventsWidgetItem = ({ item }: SoonEventsWidgetItemProps) => (
    <div className="soon-widget-item">
        <span className="soon-widget-item__num">{item.id}.</span>
        <img className="soon-widget-item__img" src={item.template} alt="widget item img" />
        <div className="soon-widget-item__event">
            <p className="soon-widget-item__title">{item.name}</p>
            <span className="soon-widget-item__place">
                {item.placement.city} - <b>{item.placement.name}</b>
            </span>
            <Link to={item.uri}>купить</Link>
        </div>

        <div className="soon-widget-item__info">
            <span className="soon-widget-item__event-date">
                {new Date(item.date).getFullYear()}
            </span>
            <span className="soon-widget-item__event-type">{item.eventType}</span>
        </div>
    </div>
);

const SoonEventsWidget = ({ items }: SoonEventsWidgetProps) => (
    // const tabsItems = [
    //     {
    //         name: 'concert',
    //         label: 'Concert'c
    //     },
    //     {
    //         name: 'party',
    //         label: 'Party'
    //     },
    //     {
    //         name: 'night_club',
    //         label: 'Night club'
    //     },
    // ];

    <article className="soon-widget">
        <h3>Soon</h3>

        {/* <Tabs activeTab={'concert'} setActiveTab={() => {}} items={tabsItems}/> */}
        <div className="soon-widget__list">
            {items.map((item, index) => (
                <SoonEventsWidgetItem key={item.uri} item={item} />
            ))}
        </div>
    </article>
);
export default SoonEventsWidget;
