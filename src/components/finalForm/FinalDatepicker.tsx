import React from 'react';
import { FieldRenderProps } from 'react-final-form';
import DatePicker from 'react-datepicker';
import { FieldWrapper } from '../fieldWrapper/FieldWrapper';
import './styles/FinalDatepicker.scss';
import { Input } from '../input/Input';

type FinalDatepickerProps = FieldRenderProps<any> & {
    ico: string;
    label: string;
};

export const FinalDatepicker = ({ input, meta, ico, label, ...rest }: FinalDatepickerProps) => {
    const CustomInput = ({ value, onClick }: any) => (
        <div onClick={onClick}>
            <Input defaultValue={value} {...rest} />
        </div>
    );
    return (
        <FieldWrapper label={label}>
            <DatePicker
                selected={input.value ? new Date(input.value) : undefined}
                onChange={(date: any) => {
                    input.onChange(date);
                }}
                // @ts-ignore
                customInput={<CustomInput />}
            />
        </FieldWrapper>
    );
};
