import React, { useCallback, useEffect, useState } from 'react';
import { FieldRenderProps } from 'react-final-form';
import cn from 'classnames';
import { useBoolean } from '../../utils/useBoolean';
import './styles/FinalPhotoUploader.scss';

type FinalPhotoUploaderProps = FieldRenderProps<any> & {
    accept: string;
};

export const FinalPhotoUploader = ({ input, meta, accept, ...rest }: FinalPhotoUploaderProps) => {
    const [isHighlighted, setHighlightedOn, setHighlightedOff] = useBoolean(false);
    const [previewImage, setPreviewImage] = useState(input.value);

    useEffect(() => {
        if (!previewImage) {
            setPreviewImage(input.value);
        }
    }, [input.value]);
    const handleChange = useCallback(
        (e) => {
            setPreviewImage(URL.createObjectURL(e.target.files[0]));
            input.onChange(e.target.files[0]);
        },
        [input.onChange]
    );

    return (
        <div className={cn('avatar-uploader', { 'is-highlighted': isHighlighted })}>
            <div className="avatar-uploader__mask">
                <input
                    type="file"
                    id={input.name}
                    accept={accept}
                    onChange={handleChange}
                    onDrag={setHighlightedOff}
                    onDragLeave={setHighlightedOff}
                    onDragOver={setHighlightedOn}
                    onDrop={setHighlightedOff}
                />
                {previewImage && (
                    <img className="avatar-uploader__preview" src={previewImage} alt="preview" />
                )}
            </div>

            <div className="avatar-uploader__buttons">
                <div
                    onClick={() => {
                        input.onChange(null);
                        setPreviewImage(null);
                    }}
                >
                    Reset
                </div>

                <label htmlFor={input.name} className="button button_secondary">
                    Select
                </label>
            </div>
        </div>
    );
};
