import React from 'react';
import { FieldRenderProps } from 'react-final-form';
import cn from 'classnames';
import './styles/FinalMultipleCheckbox.scss';
import { FieldWrapper } from '../fieldWrapper/FieldWrapper';

type MultipleCheckboxItemProps = {
    id: number;
    label: string;
    active?: boolean;
};

type FinalMultipleCheckboxProps = FieldRenderProps<any> & {
    options: Array<MultipleCheckboxItemProps>;
    label: string;
};

export const FinalMultipleCheckbox = ({
    input,
    meta,
    label,
    options,
}: FinalMultipleCheckboxProps) => (
    <FieldWrapper label={label}>
        <div className="multiple-checkbox">
            {options.map((item) => (
                <div
                    key={item.id}
                    className={cn('multiple-checkbox__item', { active: item.active })}
                    onClick={() => {
                        item.active = !item.active;
                        input.onChange(options.filter((item) => item.active));
                    }}
                >
                    {item.label}
                </div>
            ))}
        </div>
    </FieldWrapper>
);
