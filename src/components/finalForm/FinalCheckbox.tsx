import React from 'react';
import { FieldRenderProps } from 'react-final-form';
import './styles/FinalCheckbox.scss';

export const FinalCheckbox = ({
    input,
    meta,
    label,
}: FieldRenderProps<any> & { label: string }) => (
    <label className="checkbox">
        <input
            onChange={(e) => input.onChange(e.target.checked)}
            checked={input.value}
            className="hidden"
            type="checkbox"
        />
        <div className="checkbox__custom">
            {' '}
            <span className="checkbox__checked" />{' '}
        </div>
        {label}
    </label>
);
