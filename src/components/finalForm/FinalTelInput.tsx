import React from 'react';
import { FieldRenderProps } from 'react-final-form';
import PhoneInput from 'react-phone-input-2';
import { FieldWrapper } from '../fieldWrapper/FieldWrapper';
import 'react-phone-input-2/lib/style.css';
import './styles/FinalTelInput.scss';

type FinalTelInputProps = FieldRenderProps<any> & {};

export const FinalTelInput = ({ input, meta, label }: FinalTelInputProps) => (
    <FieldWrapper label={label}>
        <PhoneInput
            inputClass="tel-field__input"
            containerClass="tel-field"
            buttonClass="tel-field__flag"
            country="us"
            value={input.value}
            onChange={(phone) => {
                input.onChange(phone);
            }}
        />
    </FieldWrapper>
);
