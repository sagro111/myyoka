import React, { useState } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { mdiChevronDown, mdiChevronUp } from '@mdi/js';
import { FieldWrapper } from '../fieldWrapper/FieldWrapper';
import { Input } from '../input/Input';
import { useOnClickOutside } from '../../utils/useClickOutside';
import './styles/FinalSelect.scss';

type SelectOptionsItemProps = {
    id: number;
    label: string;
};

type SelectProps = FieldRenderProps<any> & { options: Array<SelectOptionsItemProps> };

export const FinalSelect = ({ input, meta, options = [], label }: SelectProps) => {
    const [isShowDropdown, setIsShowDropdown] = useState<boolean>(false);
    const { ref } = useOnClickOutside(() => setIsShowDropdown(false));

    const handleChange = (item: SelectOptionsItemProps) => {
        setIsShowDropdown(false);
        input.onChange(item);
    };

    return (
        <FieldWrapper label={label}>
            <Input
                {...input}
                readOnly
                placeholder="Select a value"
                defaultValue={input.value.label}
                onClick={() => setIsShowDropdown(true)}
                value={input.value.label || ' '}
                ico={isShowDropdown ? mdiChevronUp : mdiChevronDown}
            />
            {isShowDropdown && (
                <ul ref={ref} className="select-dropdown">
                    {options.map((item: SelectOptionsItemProps) => (
                        // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
                        <li
                            className="select-dropdown__item"
                            onClick={() => handleChange(item)}
                            key={item.id}
                        >
                            {item.label}
                        </li>
                    ))}
                </ul>
            )}
        </FieldWrapper>
    );
};
