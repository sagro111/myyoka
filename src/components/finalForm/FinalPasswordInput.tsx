import React, { useState } from 'react';
import { FieldRenderProps } from 'react-final-form';
import { FieldWrapper } from '../fieldWrapper/FieldWrapper';
import { Input } from '../input/Input';
import { PrimaryIcon } from '../ui/icon';
import { mbEyeClosed, mbEyeOpen } from '../ui/icon/Icons';

type FinalInputProps = FieldRenderProps<any> & {
    label: string;
    type?: 'text' | 'password';
};

export const FinalInputPassword = ({ input, meta, label, type, ...rest }: FinalInputProps) => {
    const [isPassVisible, setIsPassVisible] = useState(false);
    const { error, submitError } = meta || ({} as any);

    return (
        <FieldWrapper id={input.name} label={label}>
            <div className="password">
                <Input
                    id={input.name}
                    {...input}
                    error={error || submitError}
                    {...rest}
                    type={isPassVisible ? 'text' : 'password'}
                />
                <span className="ico" onClick={() => setIsPassVisible(!isPassVisible)}>
                    {!isPassVisible ? (
                        <PrimaryIcon size={18} icon={mbEyeOpen} />
                    ) : (
                        <PrimaryIcon icon={mbEyeClosed} size={18} />
                    )}
                </span>
            </div>
        </FieldWrapper>
    );
};
