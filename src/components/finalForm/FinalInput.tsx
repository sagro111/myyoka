import React from 'react';
import { FieldRenderProps } from 'react-final-form';
import { Input } from '../input/Input';
import { FieldWrapper } from '../fieldWrapper/FieldWrapper';

export const FinalInput = ({
    input,
    meta,
    label,
    ico,
    ...rest
}: FieldRenderProps<any> & { label: string }) => {
    const { error, submitError } = meta || ({} as any);
    return (
        <FieldWrapper id={input.name} label={label}>
            <Input {...input} error={error || submitError} id={input.name} {...rest} />
        </FieldWrapper>
    );
};
