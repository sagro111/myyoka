import React, { ReactElement } from 'react';
import './styles/FieldWrapper.scss';
import cn from 'classnames';

type FieldWrapperProps = {
    children: ReactElement | Array<ReactElement>;
    label: string;
    id?: string;
    className?: string;
};

export const FieldWrapper = ({ children, id, className, label }: FieldWrapperProps) => (
    <div className={cn(['field-wrapper', className])}>
        <label className="field-wrapper__label" htmlFor={id}>
            {label}
        </label>
        {children}
    </div>
);
