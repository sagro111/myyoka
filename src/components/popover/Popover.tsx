import React, { ReactElement, useEffect, useState } from 'react';
import cn from 'classnames';
import './styles/Popover.scss';

type PopoverProps = {
    children: ReactElement;
    onClose: () => void;
    title?: string;
    size?: 'sm' | 'md' | 'lg' | 'auto';
};

export const Popover = ({ children, onClose, title, size = 'auto' }: PopoverProps) => {
    const [isActive, setIsActive] = useState<boolean>(false);

    useEffect(() => {
        setIsActive(true);
    }, []);

    return (
        <div
            onClick={() => {
                setIsActive(false);
                setTimeout(onClose, 300);
            }}
            className={cn('popover', { popover_show: isActive })}
        >
            <div
                onClick={(event) => {
                    event.stopPropagation();
                }}
                className={cn('popover__content', { [`popover__content_${size}`]: true })}
            >
                {title && <div className="popover__title">{title}</div>}

                <div>{children}</div>
            </div>
        </div>
    );
};
