import React, { useEffect, useState } from 'react';
import './styles/ProgressCircle.scss';

type ProgressCircleProps = {
    progress: number;
    stroke: number;
    radius: number;
};

const ProgressCircle = ({ progress, radius, stroke }: ProgressCircleProps) => {
    const [value, setValue] = useState(0);
    useEffect(() => {
        setTimeout(() => setValue(progress), 500);
    }, []);

    const normalizedRadius = radius - stroke + stroke / 2;
    const circumference = normalizedRadius * 2 * Math.PI;

    const strokeDashoffset = circumference - (value / 100) * circumference;

    return (
        <div style={{ height: radius * 2, width: radius * 2 }} className="progress-circle">
            <svg height={radius * 2} width={radius * 2}>
                <circle
                    stroke="white"
                    fill="transparent"
                    strokeWidth={stroke}
                    strokeDasharray={`${circumference} ${circumference}`}
                    style={{ strokeDashoffset }}
                    r={normalizedRadius}
                    cx={radius}
                    cy={radius}
                />
            </svg>

            <span>{progress}%</span>
        </div>
    );
};

export default ProgressCircle;
