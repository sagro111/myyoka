import React, { useEffect, useState } from 'react';
import './styles/SettingsProgress.scss';
import cn from 'classnames';
import ProgressCircle from '../progressCircle/ProgressCircle';
import { Button } from '../ui/button/Button';

type SettingsProgressProps = {
    percent: number;
};

export const SettingsProgress = ({ percent }: SettingsProgressProps) => {
    const [isVisible, setIsVisible] = useState(false);
    useEffect(() => {
        setTimeout(() => setIsVisible(true), 300);
    }, []);
    return (
        <div className="profile-progress">
            <div className={cn('profile-progress__content', { show: isVisible })}>
                <ProgressCircle progress={percent} radius={40} stroke={3} />
                <div className="profile-progress__info">
                    <p className="profile-progress__title">Profile Informations</p>
                    <p className="profile-progress__desc">
                        Complete your profile to unlock all feature
                    </p>
                </div>
                <Button label="Complete My Profile" view="tertiary" />
            </div>
        </div>
    );
};
