import React from 'react';
import { FieldRenderProps } from 'react-final-form';
import PartyImg from './styles/party.jpg';
import ConcertImg from './styles/concert.jpg';
import NightClubImg from './styles/nightClub.jpg';
import './styles/SelectEventTypeAdapter.scss';

type SelectEventTypeAdapterProps = FieldRenderProps<any> & {
    onClick: () => void;
};

export const SelectEventTypeAdapter = ({ input, meta, onClick }: SelectEventTypeAdapterProps) => {
    const selectType = (type: 'party' | 'nightClub' | 'concert') => {
        input.onChange(type);
        onClick();
    };

    return (
        <div className="select-event-type">
            <div onClick={() => selectType('party')} className="select-event-type__item">
                <div className="select-event-type__img">
                    <img src={PartyImg} alt="party" />
                </div>

                <p className="select-event-type__title">Party</p>
            </div>
            <div onClick={() => selectType('nightClub')} className="select-event-type__item">
                <div className="select-event-type__img">
                    <img src={NightClubImg} alt="nightClub" />
                </div>

                <p className="select-event-type__title">Night club</p>
            </div>

            <div onClick={() => selectType('concert')} className="select-event-type__item">
                <div className="select-event-type__img">
                    <img src={ConcertImg} alt="concert" />
                </div>
                <p className="select-event-type__title">Concert</p>
            </div>
        </div>
    );
};
