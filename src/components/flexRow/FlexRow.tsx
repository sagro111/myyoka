import React, { ReactElement } from 'react';
import './styles/FlexRow.scss';

type FlexRowProps = {
    columns: number;
    children: ReactElement | Array<ReactElement>;
};

export const FlexRow = ({ columns = 1, children }: FlexRowProps) => (
    <div className={`flex-row flex-row_columns-${columns}`}>{children}</div>
);
