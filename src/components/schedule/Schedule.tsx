import React from 'react';
import './schedule.scss';
import { ScheduleItemProps } from '../../models/ConcertEventModel';

type ScheduleProps = {
    items: Array<ScheduleItemProps>;
};

const Schedule = ({ items }: ScheduleProps) => (
    <div className="schedule-list">
        {items &&
            items.map(({ time, label }) => (
                <div key={time} className="schedule-list__item">
                    <span> {time} - </span> <b>{label}</b>
                </div>
            ))}
    </div>
);

export default Schedule;
