import React from 'react';
import './styles/EventCardPreset.scss';
import cn from 'classnames';

type EventCardPresetProps = {
    children: any;
    onClick?: () => void;
    className?: string;
};

export const EventCardPreset = ({ children, onClick, className }: EventCardPresetProps) => (
    <div
        onClick={(e) => {
            e.preventDefault();
            if (onClick) {
                onClick();
            }
        }}
        className={cn(['event-preset', [className]])}
    >
        {children}
    </div>
);
