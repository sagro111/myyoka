import React, { useState } from 'react';
import './styles/menu.scss';
import MenuItem from './MenuItem';

type MenuItemProps = {
    icon: any;
    link: string;
    accessible?: boolean;
};

type MenuProps = {
    menuItems: Array<MenuItemProps>;
};

export const Menu = ({ menuItems }: MenuProps) => {
    const [whichActive, setWhichActive] = useState<string>('');
    return (
        <menu className="menu">
            {menuItems.map(
                (item) =>
                    item.accessible && (
                        <MenuItem key={item.link} setWhichActive={setWhichActive} {...item} />
                    )
            )}
        </menu>
    );
};
