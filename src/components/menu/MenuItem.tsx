import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import classNames from 'classnames';
import { PrimaryIcon } from '../ui/icon';
import './styles/menuItem.scss';

type MenuItemProps = {
    icon: string;
    link: string;
    setWhichActive: (link: string) => void;
};

const MenuItem = ({ icon, link, setWhichActive }: MenuItemProps) => {
    const history = useHistory();
    const menuClasses = classNames({
        'menu-item': true,
        active: history.location.pathname === link,
    });
    return (
        <Link onClick={() => setWhichActive(link)} className={menuClasses} to={link}>
            <PrimaryIcon icon={icon} />
        </Link>
    );
};

export default MenuItem;
