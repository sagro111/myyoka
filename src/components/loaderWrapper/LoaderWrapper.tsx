import React, { ReactElement } from 'react';
import { Loader } from '../loader';

type LoaderWrapperProps = {
    isLoading: boolean;
    children?: ReactElement;
    override?: boolean;
};

export const LoaderWrapper = ({ children, isLoading, override = true }: LoaderWrapperProps) => {
    if (override && isLoading) {
        return (
            <>
                <Loader />
                {children}
            </>
        );
    }
    return isLoading ? <Loader /> : children;
};
