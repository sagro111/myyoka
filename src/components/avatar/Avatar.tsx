import React from 'react';
import './styles/Avatar.scss';
import cn from 'classnames';

export const Avatar = ({ img }: { img: string }) => (
    <div className={cn('avatar', { avatar_empty: !img })}>
        <img className="avatar__img" src={img} alt="avatar" />
    </div>
);
