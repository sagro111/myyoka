import React, { InputHTMLAttributes } from 'react';
import './styles/Input.scss';
import cn from 'classnames';
import Icon from '@mdi/react';

// eslint-disable-next-line no-undef
type InputProps = InputHTMLAttributes<HTMLInputElement> & { error?: any; ico?: any };

export const Input = ({ error, ico, ...props }: InputProps) => (
    <div className="input">
        {ico && <Icon className="input__ico" size={1} path={ico} />}
        <input
            className={cn('input__field', { input__field_error: error })}
            type="text"
            {...props}
        />
    </div>
);
