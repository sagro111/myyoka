import React from 'react';
import './loader.scss';

export const Loader = () => (
    <div className="loader">
        <div className="loader-content">
            <div> </div>
            <div> </div>
            <div> </div>
        </div>
    </div>
);
