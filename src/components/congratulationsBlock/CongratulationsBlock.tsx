import React, { useEffect } from 'react';
import './styles/CongratulationsBlock.scss';

type CongratulationsBlockProps = {
    email: string;
    close: () => void;
};

export const CongratulationsBlock = ({
    email = 'admin@admin.com',
    close,
}: CongratulationsBlockProps) => {
    useEffect(() => {
        setTimeout(() => close(), 3000);
    }, []);
    return (
        <div className="congratulations-block">
            <p className="congratulations-block__title">Congratulations</p>
            <p>We have sent verification mail to your email</p>
            <p className="congratulations-block__email">
                <b>{email}</b>
            </p>
            <p>You just have to confirm your mail</p>
        </div>
    );
};
