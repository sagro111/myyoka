import React from 'react';
import { Button } from '../ui/button/Button';
import './styles/FormButtons.scss';

export const FormButtons = ({ onReset, onSubmit }: any) => (
    <div className="form-buttons">
        <Button view="transparent" label="Clear" onClick={onReset} type="reset" />
        <Button view="secondary" label="Submit" onClick={onSubmit} type="submit" />
    </div>
);
