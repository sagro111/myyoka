import React, { useState } from 'react';
import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import './styles/RangeCalendar.scss';
import { DateRange } from 'react-date-range';
// @ts-ignore
import * as locales from 'react-date-range/dist/locale';

type RangeCalendarProps = {
    onSelectDate: (values: any) => void;
};

export const RangeCalendar = ({ onSelectDate }: RangeCalendarProps) => {
    const [state, setState] = useState({
        startDate: new Date(),
        endDate: new Date(),
        key: 'selection',
    });

    return (
        <div>
            <DateRange
                editableDateInputs
                showDateDisplay={false}
                showMonthArrow={false}
                showMonthAndYearPickers={false}
                locale={locales[navigator.languages[1]]}
                // @ts-ignore
                onChange={(item) => setState(item?.selection)}
                moveRangeOnFirstSelection={false}
                ranges={[state]}
                classNames={{
                    nextButton: 'custom-arrow',
                }}
            />
        </div>
    );
};
