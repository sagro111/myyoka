import React, { ReactElement } from 'react';
import './styles/Tooltip.scss';

type TooltipProps = {
    positions: {
        left?: string;
        right?: string;
        top?: string;
        bottom?: string;
    };
    children: ReactElement | Array<ReactElement>;
};

export const Tooltip = ({ children, positions }: TooltipProps) => (
    <div style={positions} className="tooltip">
        {children}
    </div>
);
