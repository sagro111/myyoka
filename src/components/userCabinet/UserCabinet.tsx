import React, { useEffect, useState } from 'react';
import './styles/UserCabinet.scss';
import { mdiChevronDown } from '@mdi/js';
import Icon from '@mdi/react';
import { Link, useLocation } from 'react-router-dom';
import { Avatar } from '../avatar/Avatar';
import { IUserShortModel } from '../../models/User';
import { AuthContext } from '../../store/mobx/AuthStore';
import { Tooltip } from '../tooltip/Tooltip';
import { PrimaryIcon } from '../ui/icon';
import { mbSettingsIcon } from '../ui/icon/Icons';
import { useOnClickOutside } from '../../utils/useClickOutside';

type UserCabinetProps = {
    logout: () => void;
    onClick: (context: AuthContext) => void;
    user: IUserShortModel;
};

export const UserCabinet = ({ user, onClick, logout }: UserCabinetProps) => {
    const [isVisibleDropdown, setIsVisibleDropdown] = useState(false);
    const { ref } = useOnClickOutside(() => setIsVisibleDropdown(false));
    const location = useLocation();

    useEffect(() => {
        setIsVisibleDropdown(false);
    }, [location.pathname]);

    return (
        <div className="user-cabinet">
            {user ? (
                <div className="user-cabinet__info">
                    <p className="user-cabinet__name">{user.username}</p>
                    <Avatar img={user.avatar} />
                    <div ref={ref}>
                        <span
                            className="dropdown"
                            onClick={() => setIsVisibleDropdown(!isVisibleDropdown)}
                        >
                            <Icon path={mdiChevronDown} size={1} />
                        </span>
                        {isVisibleDropdown && (
                            <Tooltip positions={{ right: '24px', bottom: '0' }}>
                                <div className="user-cabinet__dropdown">
                                    <Link to="/profile">
                                        <PrimaryIcon size={16} icon={mbSettingsIcon} /> Profile
                                    </Link>
                                    <Link to="/settings">
                                        <PrimaryIcon icon={mbSettingsIcon} /> Settings
                                    </Link>
                                    <Link to="/logout">
                                        <PrimaryIcon icon={mbSettingsIcon} /> Logout
                                    </Link>
                                </div>
                            </Tooltip>
                        )}
                    </div>
                </div>
            ) : (
                <span className="user-cabinet__sign-in" onClick={() => onClick('sign_up_form')}>
                    Sign In
                </span>
            )}
        </div>
    );
};
