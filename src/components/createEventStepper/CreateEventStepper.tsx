import React from 'react';
import { CreateEventContextType, CreateEventStepperItem } from '../../models/CreateEvent';
import './styles/CreateEventStepper.scss';
import { bem } from '../../utils/bem';

type CreateEventStepperProps = {
    activeStep: CreateEventContextType | unknown;
    withSingleton: boolean;
    tabs: Array<CreateEventStepperItem>;
};

const cn = bem('stepper');
const cnItem = bem('stepper-item');
export const CreateEventStepper = ({
    activeStep,
    withSingleton = true,
    tabs,
}: CreateEventStepperProps) => (
    <div
        className={cn({
            single: withSingleton,
            active: withSingleton && activeStep !== 'select_type',
        })}
    >
        {tabs.map((item, index) => (
            <div
                className={cnItem({ active: activeStep === item.activeStep })}
                key={item.activeStep}
            >
                {index + 1}
                <span className={cnItem('label')}>{item.label}</span>
            </div>
        ))}
    </div>
);
