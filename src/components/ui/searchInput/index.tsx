import React from 'react';
import classNames from 'classnames';
import Ico from '@mdi/react';
import { mdiMagnify } from '@mdi/js';

type SearchInputProps = {
    isActive: boolean;
    setActive?: () => void;
    enablePlaceHolder?: boolean;
};

export const SearchInput = ({
    isActive,
    setActive,
    enablePlaceHolder = false,
}: SearchInputProps) => {
    const searchClass = classNames({
        search: true,
        active: isActive,
    });

    return (
        <div className={searchClass}>
            <input type="text" />

            <span className="search__ico" onClick={setActive}>
                <Ico path={mdiMagnify} color="white" />
                {enablePlaceHolder && 'Поиск'}
            </span>
        </div>
    );
};
