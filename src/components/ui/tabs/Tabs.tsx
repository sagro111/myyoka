import React from 'react';
import classNames from 'classnames';
import { ChatChanelTypeProps } from '../../../store/mobx/ChatStore';
import './tabs.scss';

type ChatPageTabsProps = {
    onClick: (tabsType: ChatChanelTypeProps) => void;
    tabs: Array<TabsType>;
    currentTab: string;
};

type TabsType = {
    label: string;
    type: ChatChanelTypeProps;
};

export const Tabs = ({ tabs, onClick, currentTab }: ChatPageTabsProps) => (
    <div className="chat-tabs">
        {tabs.length > 0 &&
            tabs.map((item) => {
                const chatTabsItemClasses = classNames({
                    'chat-tabs__item': true,
                    active: currentTab === item.type,
                });

                return (
                    <div
                        className={chatTabsItemClasses}
                        key={item.type}
                        onClick={() => onClick(item.type)}
                    >
                        {item.label}
                    </div>
                );
            })}
    </div>
);
