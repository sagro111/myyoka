import React from 'react';
import './icons.scss';

type IconProps = {
    icon: string;
    size?: number;
};

export const PrimaryIcon = ({ icon, size = 16 }: IconProps) => (
    <svg viewBox={`0 0 ${size} ${size}`} xmlns="http://www.w3.org/2000/svg">
        <path d={icon} />
    </svg>
);
