import React from 'react';

type ImageProps = {
    src: string;
    alt: string;
};

export const Image = ({ src, alt, ...rest }: ImageProps) => (
    <img src={src || '/placeholder.png'} alt={alt} {...rest} />
);
