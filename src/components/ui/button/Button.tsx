import React, { ButtonHTMLAttributes } from 'react';
import './styles/Button.scss';
import { bem } from '../../../utils/bem';

type ButtonProps = ButtonHTMLAttributes<any> & {
    label: string;
    view: 'primary' | 'secondary' | 'tertiary' | 'transparent';
    isSquare?: boolean;
};

const cn = bem('button');
export const Button = ({
    label,
    type,
    view = 'primary',
    isSquare = false,
    ...rest
}: ButtonProps) => (
    <button type="submit" className={cn({ [view]: true, square: isSquare })} {...rest}>
        {label}
    </button>
);
