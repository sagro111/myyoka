import React from 'react';
import { Link } from 'react-router-dom';
import './styles/EventCard.scss';
import { mdiHeartOutline } from '@mdi/js';
import Icon from '@mdi/react';
import { EventCardPreset } from '../eventCardPreset/EventCardPreset';

type EventCardProps = {
    image: string;
    minAge: number;
    title: string;
    date: string;
    placement: string;
    link: string;
    price: string;
};

const EventCard = ({ title, image, date, link, price, minAge, placement }: EventCardProps) => (
    <Link to={link} className="event-card">
        <div className="event-card__image">
            <img src={image} alt="event avatar" />

            <div className="event-card__features">
                {price && <EventCardPreset className="price">от {price} ₽</EventCardPreset>}
                <EventCardPreset> {minAge}+</EventCardPreset>
                <EventCardPreset>
                    {' '}
                    <Icon size={1} path={mdiHeartOutline} />{' '}
                </EventCardPreset>
            </div>
        </div>

        <div className="event-card__info">
            <p className="event-card__title">{title}</p>
            <div className="event-card__desc">
                <p>{date}</p>
                <span className="dot">•</span>
                <p>{placement}</p>
            </div>
        </div>
    </Link>
);

export default EventCard;
