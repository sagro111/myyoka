import React from 'react';
import classNames from 'classnames';
import './styles/logo.scss';

type LogoProps = {
    size: 'small' | 'full';
};

const Logo = ({ size }: LogoProps) => {
    const logoClasses = classNames({
        logo: true,
        [size]: true,
    });

    return <div className={logoClasses} />;
};

export default Logo;
