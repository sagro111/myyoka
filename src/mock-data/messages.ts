export const messages = [
    {
        id: '1',
        name: 'Роман',
        surname: 'Кузьменко',
        update_at: 'Mon May 25 2020 21:23:33 GMT+0300',
        lastMessage: 'Привет',
    },
    {
        id: '2',
        name: 'Игорь',
        surname: 'Латышев',
        update_at: 'Mon May 24 2020 21:23:33 GMT+0300',
        lastMessage: 'Я это ты',
    },
    {
        id: '3',
        name: 'Даниил',
        surname: 'Зайцев',
        update_at: 'Mon May 25 2020 23:23:33 GMT+0300',
        lastMessage: 'надо сегодня собраться :)',
    },
];
