export interface StoreInterface<Context> {
    isLoading: boolean;
    setIsLoading: (val: boolean) => void;
    context: Context;
    changeContext: (val: Context) => void;
}
