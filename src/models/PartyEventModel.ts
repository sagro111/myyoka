import { IEventModel } from './index';

type PartyEventModel = {
    eventType: 'party';
};

export type PartyEventProps = PartyEventModel & IEventModel;
