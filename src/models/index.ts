import { PartyEventProps } from './PartyEventModel';
import { NightClubEventProps } from './NightClubEventModel';
import { ConcertEventProps } from './ConcertEventModel';

export type IEventModel = {
    id: number;
    template: string;
    avatar: string;
    name: string;
    placement: {
        name: string;
        city: string;
        lngLtd: string;
    };
    uri: string;
    date: string;
};

export type EventsCommonTypes = PartyEventProps | NightClubEventProps | ConcertEventProps;
