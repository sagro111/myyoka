import { IAddress } from './Auth';

export type IUserShortModel = {
    _id: string;
    status: string;
    role: Array<string>;
    username: string;
    avatar?: string;
    exp: number;
};

export type IUserModel = {
    readonly email: string;
    readonly avatar: string;
    readonly avatarId: string;
    readonly lastName: string;
    readonly firstName: string;
    readonly gender: string;
    readonly address: IAddress;
    readonly role: Array<string>;
};

export type IReadableUserModel = IUserModel & {
    accessToken: string;
};

export enum StatusEnum {
    pending = 'pending',
    active = 'active',
    blocked = 'blocked',
}
