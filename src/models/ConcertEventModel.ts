import { IEventModel } from './index';

export type ConcertEventProps = ConcertEventModel & IEventModel;

type ConcertEventModel = {
    eventType: 'concert';
    schedule: Array<ScheduleItemProps>;
};

export type ScheduleItemProps = {
    time: string;
    label: string;
};
