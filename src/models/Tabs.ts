export type TabsType = {
    activeTab: string;
    setActiveTab: (name: string) => void;
    items: Array<TabsItemType>;
};

export type TabsItemType = {
    label: string;
    name: string;
};
