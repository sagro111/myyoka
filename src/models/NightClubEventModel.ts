import { IEventModel } from './index';

type NightClubEventModel = {
    eventType: 'night_club';
};

export type NightClubEventProps = NightClubEventModel & IEventModel;
