import { ReactNode } from 'react';

export type CreateEventStepperItem = {
    activeStep: CreateEventContextType;
    component: ReactNode;
    label: CreateEventStepNames;
};

export type EventType = 'concert' | 'nightClub' | 'party';

export type CreateEventContextType = 'select_type' | 'main_info' | 'time_placement' | 'media';

export type CreateEventStepNames = 'Select type' | 'Main info' | 'Time and Placement' | 'Media';
