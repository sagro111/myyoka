export type SignUpProps = {
    readonly email: string;
    readonly age: number;
    readonly password: string;
    readonly avatar: string;
    readonly avatarId: string;
    readonly lastName: string;
    readonly firstName: string;
    readonly gender: string;
    readonly address: IAddress;
};

export type IAddress = {
    readonly country: string;
    readonly city: string;
};
