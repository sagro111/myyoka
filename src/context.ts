import { createContext, useContext } from 'react';
import { assert } from 'ts-essentials';
import RootStore from './store/mobx';

export const StoreContext = createContext<RootStore | null>(null);

export const useStore = () => {
    const context = useContext(StoreContext);
    assert(context);
    return context;
};
