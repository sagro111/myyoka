import { FORM_ERROR, SubmissionErrors } from 'final-form';
import { isEmpty } from 'lodash';

export const handleFormSubmit = async <T>(
    request: Promise<T>
): Promise<{ errors: SubmissionErrors | null; response: T | null }> => {
    try {
        const response = await request;
        return { response, errors: null };
    } catch (err) {
        if (!err.response || !err.response.data) {
            return {
                response: null,
                errors: { [FORM_ERROR]: 'Server side error. Please contact the support.' },
            };
        }
        return { errors: errorProcess(err.response.data), response: null };
    }
};

function errorProcess(responseData: any) {
    if (responseData.statusCode && responseData.statusCode === 401) {
        return { [FORM_ERROR]: responseData.message };
    }
    const res: any = {};
    if (responseData && responseData.message) {
        return responseData.message.reduce((acc: Array<any>, item: any) => {
            const key = Object.keys(item)[0];
            return { ...acc, [key]: item[key] };
        }, []);
    }
    if (responseData && responseData.error) {
        res[FORM_ERROR] = 'Server side error. Please contact the support.';
    }

    return isEmpty(res) ? null : res;
}
