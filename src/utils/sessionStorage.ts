import { AnyObject } from 'final-form';

type KeyType = 'create_event';

export const saveDataToStorage = (key: KeyType, values: AnyObject): void => {
    localStorage.setItem(key, JSON.stringify(values));
};

export const clearSessionStorage = (key: KeyType): void => {
    localStorage.removeItem(key);
};

export const getSessionStorageData = async (key: KeyType): Promise<AnyObject> => {
    try {
        return await JSON.parse(localStorage.getItem(key));
    } catch (e) {
        return e;
    }
};
