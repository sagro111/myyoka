export const parseHistoryUrlToUri = (str: string) => str.split('?').join('/').split('=').join('/');
