import { useLocation } from 'react-router';

export const useQuery = (options: string) => new URLSearchParams(useLocation().search).get(options);
