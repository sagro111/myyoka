import { EventType } from '../models/CreateEvent';

export const getEventTypeFromUrl = (str: string = ''): EventType =>
    str.split('?')[1].split('=')[0] as EventType;
