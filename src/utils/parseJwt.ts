type ParseJwtResponse = {
    _id: string;
    exp: number;
    username: string;
    avatar: string;
    status: string;
    role: Array<string>;
};

export const parseJwt = (token: string): ParseJwtResponse => {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(
        atob(base64)
            .split('')
            .map((c) => `%${`00${c.charCodeAt(0).toString(16)}`.slice(-2)}`)
            .join('')
    );

    return JSON.parse(jsonPayload);
};

export function isTokenExpired(token: string): boolean {
    const date = getTokenExpirationDate(token);
    const offsetSeconds = 0;
    if (date.getTime() - new Date().getTime() < 0) {
        return true;
    }
    return !(date.valueOf() > new Date().valueOf() + offsetSeconds * 1000);
}

function getTokenExpirationDate(token: string) {
    const result = new Date(0); // The 0 here is the key, which sets the date to the epoch
    if (!token) {
        return result;
    }
    try {
        const decoded = parseJwt(token);
        if (!decoded.exp) {
            return result;
        }
        result.setUTCSeconds(decoded.exp);
        return result;
    } catch (e) {
        return new Date('01-01-1970');
    }
}
