export const idFromEntity = (str: string): string => str.split('/').reverse()[0];
