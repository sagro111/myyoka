import React from 'react';
import { render } from 'react-dom';
import axios from 'axios';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import RootStore from './store/mobx';
import ApiClient from './store/ApiClient';
import { StoreContext } from './context';
import App from './containers/app/App';
import './css/style.scss';

const browserHistory = createBrowserHistory();

const routingStore = new RouterStore();
const history = syncHistoryWithStore(browserHistory, routingStore);

const rootEl = document.getElementById('root');

const mobxStore = new RootStore(new ApiClient(axios, routingStore), routingStore) as RootStore;
const renderApp = (store: RootStore) => {
    render(
        <StoreContext.Provider value={store}>
            <Router history={history}>
                <App />
            </Router>
        </StoreContext.Provider>,
        rootEl
    );
};

renderApp(mobxStore);
