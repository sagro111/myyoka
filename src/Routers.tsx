import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { HomePage } from './containers/home/HomePage';
import { MapsPage } from './containers/maps/MapsPage';
import { CalendarPage } from './containers/calendar/CalendarPage';
import { ChatPage } from './containers/chat/ChatPage';
import { Impersonate } from './containers/impersonate/Impersonate';
import { SettingsPage } from './containers/settingsPage/SettingsPage';
import { LogoutPage } from './containers/logoutPage/LogoutPage';
import { CreateEvent } from './containers/createEvent/CreateEvent';

export const Router = () => (
    <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/create_event" component={CreateEvent} />
        <Route exact path="/calendar" component={CalendarPage} />
        <Route exact path="/_impersonate" component={Impersonate} />
        <Route exact path="/settings" component={SettingsPage} />
        <Route exact path="/logout" component={LogoutPage} />
        <Route exact path="/maps" component={MapsPage} />
        <Route exact path="/chat" component={ChatPage} />
        <Route exact path="/chat/:userId" component={ChatPage} />
    </Switch>
);
