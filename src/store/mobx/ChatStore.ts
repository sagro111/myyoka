import { makeAutoObservable } from 'mobx';
import ApiClient from '../ApiClient';

export type ChatChanelTypeProps = 'companies' | 'contacts';

export type ChatListProps = {
    avatar: string;
    name: string;
    surname: string;
    date: string;
    lastMessage: string;
    id: string;
};

export default class ChatStore {
    chanelType: ChatChanelTypeProps = 'companies';

    chatList: Array<ChatListProps> = [];

    isLoading: boolean = false;

    activeChat?: ChatListProps;

    constructor(private apiClient: ApiClient) {
        makeAutoObservable(this);
    }

    clearStore = () => {
        this.isLoading = true;
        this.chanelType = 'companies';
        this.chatList = [];
    };

    toggleLoader = () => {
        this.isLoading = !this.isLoading;
    };

    setChanelType = (chanel: ChatChanelTypeProps): void => {
        this.chanelType = chanel;
    };

    fetchChats = (chanel: ChatChanelTypeProps): void => {
        this.toggleLoader();

        setTimeout(() => {
            this.apiClient.fetchChats(chanel, 1).then((res) => {
                this.chatList = res;
                this.toggleLoader();
            });
        }, 1500);
    };

    setActiveChat = (chat: ChatListProps) => {
        this.activeChat = chat;
    };

    // @ToDO Типы
    sendMessage = (message: any) => {
        this.apiClient.sendMessage(this.chanelType, this.activeChat.id).then((res) => res);
    };

    get getChanelType(): ChatChanelTypeProps {
        return this.chanelType;
    }

    get sortedChatsList() {
        return this.chatList.sort(
            (itemB, itemA) => new Date(itemB.date).getTime() - new Date(itemA.date).getTime()
        );
    }
}
