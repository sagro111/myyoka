import { makeAutoObservable } from 'mobx';

export type NotificationItemTypes = 'error' | 'warning' | 'success' | 'info';

export type NotificationAlignTypes =
    | 'top_right'
    | 'bottom_right'
    | 'center'
    | 'top_left'
    | 'bottom_left';

export type NotificationItemModel = {
    title: string;
    message: string;
    type: NotificationItemTypes;
    align?: NotificationAlignTypes;
    duration?: number;
};

export class NotificationStore {
    notificationItem: NotificationItemModel;

    constructor() {
        makeAutoObservable(this);
    }

    addNotification = (props: NotificationItemModel) => {
        this.notificationItem = props;
        if (props.duration) {
            setTimeout(() => {
                this.notificationItem = null;
            }, props.duration);
        }
    };

    closeNotification = () => {
        this.notificationItem = null;
    };

    get getNotification() {
        return this.notificationItem;
    }
}
