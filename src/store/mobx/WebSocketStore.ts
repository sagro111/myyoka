export default class WebSocketStore {
    webSocket: WebSocket;

    constructor() {
        this.webSocket = new WebSocket('ws://localhost:3001');

        this.webSocket.onopen = (ws) => {
            this.webSocket.send('Hi server');
        };

        this.webSocket.onmessage = (res) => {};
    }
}
