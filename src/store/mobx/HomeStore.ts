import { action, makeAutoObservable } from 'mobx';
import ApiClient from '../ApiClient';
import { EventsCommonTypes } from '../../models';

export class HomeStore {
    soonEventsData: Array<EventsCommonTypes> | [] = [];

    constructor(private readonly apiClient: ApiClient) {
        makeAutoObservable(this);
    }

    public fetchEventsCollection = () => {
        this.apiClient.fetchEventsCollection().then(
            action('fetch is ok', (res) => {
                this.soonEventsData = res;
            })
        );
    };
}
