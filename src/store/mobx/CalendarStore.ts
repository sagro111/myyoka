import { makeAutoObservable } from 'mobx';
import ApiClient from '../ApiClient';

export class CalendarStore {
    party: Array<any> = partyMock;

    concert: Array<any> = concertMock;

    nighClub: Array<any>;

    isLoading: boolean;

    constructor(private apiClient: ApiClient) {
        makeAutoObservable(this);
    }

    fetchShortCollection = async (): Promise<void> => {};
}

const partyMock = [
    {
        image:
            'https://ca-times.brightspotcdn.com/dims4/default/26b035d/2147483647/strip/true/crop/5000x3334+0+0/resize/840x560!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F4d%2Faf%2Fb65bd92b4a7683c06a66a2d60772%2Fla-photos-handouts-fi-hp-top-sales-822sarbonne181.jpg',
        minAge: 16,
        title: 'LA Smiths House',
        date: '20 February',
        placement: 'Smiths House',
        link: '/party/3',
    },
    {
        image: 'https://i.insider.com/5319e2c86da811d06bb2b4ba?width=1100&format=jpeg&auto=webp',
        minAge: 16,
        title: 'LA Smiths House',
        date: '20 February',
        placement: 'Smiths House',
        link: '/party/2',
    },
    {
        image: 'https://img.ti-media.net/wp/uploads/sites/46/2016/07/kendall-jenner-house-17.jpg',
        minAge: 16,
        title: 'LA Smiths House',
        date: '20 February',
        placement: 'Smiths House',
        link: '/party/1',
    },
    {
        image: 'https://img.ti-media.net/wp/uploads/sites/46/2016/07/kendall-jenner-house-17.jpg',
        minAge: 16,
        title: 'LA Smiths House',
        date: '20 February',
        placement: 'Smiths House',
        link: '/party/1',
    },
    {
        image: 'https://img.ti-media.net/wp/uploads/sites/46/2016/07/kendall-jenner-house-17.jpg',
        minAge: 16,
        title: 'LA Smiths House',
        date: '20 February',
        placement: 'Smiths House',
        link: '/party/1',
    },
];

const concertMock = [
    {
        image:
            'https://avatars.mds.yandex.net/get-afishanew/23222/441ee8cfe36ca9334449b60fe195d756/s372x223',
        minAge: 16,
        title: 'ATL',
        date: '14 March',
        time: '20:30',
        price: '2500',
        placement: 'Adrenaline Stadium',
        link: '/party/3',
    },
    {
        image:
            'https://avatars.mds.yandex.net/get-afishanew/35821/d73978b9c3e54a5d35985038d242dea6/s372x223',
        minAge: 16,
        title: 'Noize MC',
        date: '20 February',
        price: '2500',
        placement: 'Smiths House',
        time: '21:30',
        link: '/party/2',
    },
    {
        image:
            'https://avatars.mds.yandex.net/get-afishanew/29022/bc1ec5a8a695fbe78f318184846b6adb/s372x223',
        minAge: 16,
        title: 'PHARAOH',
        date: '20 February',
        price: '1500',
        time: '21:00',
        placement: 'Smiths House',
        link: '/party/1',
    },
    {
        image: 'https://img.ti-media.net/wp/uploads/sites/46/2016/07/kendall-jenner-house-17.jpg',
        minAge: 16,
        title: 'LA Smiths House',
        price: '2500',
        date: '20 February',
        time: '19:50',
        placement: 'Smiths House',
        link: '/party/1',
    },
];
