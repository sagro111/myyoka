import { makeAutoObservable } from 'mobx';
import ApiClient from '../ApiClient';
import { StoreInterface } from '../../models/Stores';
import { IUserModel } from '../../models/User';
import { parseJwt } from '../../utils/parseJwt';
import { handleFormSubmit } from '../../utils/handleFormSubmit';

type ProfileContextTypes = 'appearance' | 'security' | 'profile';

export class SettingsStore implements StoreInterface<ProfileContextTypes> {
    context: ProfileContextTypes = 'profile';

    isLoading: boolean = false;

    user: IUserModel;

    constructor(private readonly apiClient: ApiClient, private readonly token: string) {
        makeAutoObservable(this);
        const id = parseJwt(token)._id;
        this.getProfile(id);
    }

    setIsLoading(val: boolean): void {
        this.isLoading = val;
    }

    changeContext(context: ProfileContextTypes): void {
        this.context = context;
    }

    async getProfile(_id: string) {
        this.isLoading = true;
        try {
            const { data } = await this.apiClient.getProfile(_id);
            if (data) {
                setTimeout(() => {
                    this.isLoading = false;
                    this.user = data;
                }, 400);
            }
        } catch (e) {
            console.error(e.response);
        }
    }

    updateProfile = async (values: any) => {
        this.isLoading = true;
        const id = parseJwt(this.token)._id;

        if (values.avatar && typeof values.avatar !== 'string') {
            values.avatar = await this.uploadAvatar(values);
        }

        const { response, errors } = await handleFormSubmit(
            this.apiClient.updateProfile(values, id)
        );
        if (!errors) {
            this.user = response;
        }
        this.isLoading = false;
    };

    uploadAvatar = async ({ avatar }: any): Promise<string> => {
        const formData = new FormData();
        formData.append('avatar', avatar);
        const { avatarLink } = await this.apiClient.uploadAvatar(formData);
        return avatarLink;
    };
}
