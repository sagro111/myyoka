import { SubmissionErrors } from 'final-form';
import { RouterStore } from 'mobx-react-router';
import { makeAutoObservable } from 'mobx';
import ApiClient from '../ApiClient';
import { CreateEventContextType, EventType } from '../../models/CreateEvent';
import { NotificationStore } from './NotificationStore';

export class EventStore {
    private isLoading: boolean = false;

    private currentEvent: any;

    constructor(
        private readonly apiClient: ApiClient,
        private readonly routingStore: RouterStore,
        private readonly notificationStore: NotificationStore
    ) {
        makeAutoObservable(this);
    }

    setEventContext = (context: CreateEventContextType) => {
        this.routingStore.history.push({
            ...this.routingStore.location,
            state: context,
        });
    };

    submitForm = async (values: any) => {
        const { state } = this.routingStore.location;

        if (!state) {
            const errors = await this.selectType(values.eventType);
            if (!errors) {
                this.setEventContext('main_info');
                return;
            }
            this.notificationStore.addNotification({
                type: 'error',
                title: "You don't have permission",
                message: 'Sorry, but for create this event, you can verify your access',
            });
        }
        if (state === 'main_info') {
            this.setEventContext('time_placement');
        }
        if (state === 'time_placement') {
            this.setEventContext('media');
        } else {
            console.log(values);
        }
    };

    selectType = async (type: EventType): Promise<SubmissionErrors | void> => {
        this.isLoading = true;
        const { response, errors } = await this.apiClient.selectType(type);
        if (!errors) {
            this.currentEvent = response;
        }
        this.isLoading = false;
        return errors;
    };
}
