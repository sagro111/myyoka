import { makeAutoObservable } from 'mobx';
import { SubmissionErrors } from 'final-form';
import { RouterStore } from 'mobx-react-router';
import ApiClient from '../ApiClient';
import { parseJwt } from '../../utils/parseJwt';
import { SignUpProps } from '../../models/Auth';
import { IUserShortModel } from '../../models/User';
import { StoreInterface } from '../../models/Stores';
import { handleFormSubmit } from '../../utils/handleFormSubmit';

export type SignInProps = {
    readonly email: string;
    readonly password: string;
};

export type AuthContext = 'sign_up_form';

export class AuthStore implements StoreInterface<AuthContext> {
    user: IUserShortModel;

    context: AuthContext;

    isLoading: boolean = false;

    congratsEmail: string;

    constructor(
        private readonly apiClient: ApiClient,
        private router: RouterStore,
        private token: string | null
    ) {
        makeAutoObservable(this, { parseJwtToken: false });
        if (token) {
            this.parseJwtToken(token);
        }
    }

    setIsLoading = (value: boolean): void => {
        this.isLoading = value;
    };

    changeContext = (context: AuthContext): void => {
        this.context = context;
    };

    setCongratsEmail = (email: string): void => {
        this.congratsEmail = email;
    };

    parseJwtToken = (token: string) => {
        localStorage.setItem('token', token);
        this.user = parseJwt(token);
    };

    get isAuthed(): boolean {
        return Boolean(this.user);
    }

    signUp = async (formValues: SignUpProps): Promise<SubmissionErrors | void> => {
        this.setIsLoading(true);
        const { errors } = await handleFormSubmit(this.apiClient.signUp(formValues));
        if (!errors) {
            this.setCongratsEmail(formValues.email);
        }
        this.setIsLoading(false);
        return errors;
    };

    signIn = async (values: SignInProps): Promise<SubmissionErrors | void> => {
        this.setIsLoading(true);
        const { response: token, errors } = await handleFormSubmit(this.apiClient.signIn(values));
        if (!errors) {
            this.parseJwtToken(token);
            this.changeContext(null);
        }
        this.setIsLoading(false);

        return errors;
    };

    confirm = async (token: string): Promise<void> => {
        try {
            await this.apiClient.confirm(token);
            this.router.history.push('/profile');
            this.parseJwtToken(token);
        } catch (e) {
            console.error(e.response);
        }
    };

    logout = async (): Promise<void> => {
        this.user = null;
        localStorage.removeItem('token');
    };
}
