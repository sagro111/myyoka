import { RouterStore } from 'mobx-react-router';
import { UiStore } from './UiStore';
import ApiClient from '../ApiClient';
import { AuthStore } from './AuthStore';

import { CalendarStore } from './CalendarStore';
import { HomeStore } from './HomeStore';
import ChatStore from './ChatStore';
import { SettingsStore } from './SettingsStore';
import { EventStore } from './EventStore';
import { NotificationStore } from './NotificationStore';

export default class RootStore {
    uiStore: UiStore;

    authStore: AuthStore;

    routerStore: RouterStore;

    apiClient: ApiClient;

    calendarStore: CalendarStore;

    homeStore: HomeStore;

    notificationStore: NotificationStore;

    token: string = localStorage.getItem('token');

    constructor(apiClient: ApiClient, routerStore: RouterStore) {
        this.uiStore = new UiStore();
        this.calendarStore = new CalendarStore(apiClient);
        this.homeStore = new HomeStore(apiClient);

        this.apiClient = apiClient;
        this.routerStore = routerStore;
        this.authStore = new AuthStore(apiClient, routerStore, this.token);
        this.notificationStore = new NotificationStore();
    }

    createChatStore = (): ChatStore => new ChatStore(this.apiClient);

    createSettingsStore = (): SettingsStore => new SettingsStore(this.apiClient, this.token);

    createEventStore = (): EventStore =>
        new EventStore(this.apiClient, this.routerStore, this.notificationStore);
}
