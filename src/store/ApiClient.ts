import { RouterStore } from 'mobx-react-router';
import axios, { AxiosResponse, AxiosStatic } from 'axios';
import { EventsCommonTypes } from '../models';
import { ChatChanelTypeProps, ChatListProps } from './mobx/ChatStore';
import { events } from '../mock-data/event-list';
import { messages } from '../mock-data/messages';
import { SignInProps } from './mobx/AuthStore';
import { isTokenExpired } from '../utils/parseJwt';
import { SignUpProps } from '../models/Auth';
import { IUserModel } from '../models/User';
import { EventType } from '../models/CreateEvent';

class ApiClient {
    constructor(private _client: AxiosStatic = axios, routingStore: RouterStore) {
        axios.interceptors.response.use(
            (response) => response,
            (error) => {
                if (error.response?.status === 401) {
                    routingStore.history.push('/logout');
                }
                return Promise.reject(error);
            }
        );
        _client.defaults.baseURL = process.env.REACT_APP_API_URL;
    }

    private get client() {
        const token = localStorage.getItem('token');

        if (token && !isTokenExpired(token)) {
            this._client.defaults.headers.Authorization = `Bearer ${token}`;
        }

        return this._client;
    }

    // public async checkAuth(): Promise<any> {
    //     return this.client.post('/auth');
    // }

    public async confirm(token: string): Promise<any> {
        return this.client.post('/auth/confirm', { token }).then((res) => res.data);
    }

    public async signUp(body: SignUpProps): Promise<AxiosResponse<any>> {
        return this.client.post('/auth/sign_up', body).then((res) => res);
    }

    public async signIn(body: SignInProps): Promise<string> {
        return this.client.post('/auth/sign_in', body).then((res) => res.data);
    }

    public async fetchEventsCollection(): Promise<Array<EventsCommonTypes>> {
        return new Promise<any>((resolve) => resolve(events));
    }

    public async fetchChats(
        channel: ChatChanelTypeProps,
        userId: number
    ): Promise<Array<ChatListProps>> {
        return new Promise<any>((resolve) => resolve(messages));
    }

    public sendMessage(chanel: ChatChanelTypeProps, chatId: string): Promise<any> {
        return new Promise<any>((resolve) => resolve('1'));
    }

    public getProfile(_id: any): Promise<AxiosResponse<IUserModel>> {
        return this.client.get(`/profile/${_id}`).then((res) => res);
    }

    public uploadAvatar(avatar: any): Promise<any> {
        return this.client.post(`/profile/upload_avatar`, avatar).then((res) => res.data);
    }

    public updateProfile(values: any, _id: string): Promise<any> {
        return this.client.post(`/profile/${_id}`, values).then((res) => res.data);
    }

    public async selectType(type: EventType): Promise<any> {
        return type === 'party'
            ? { response: { id: 1, type: 'party' } }
            : { errors: 'Dont have permissions' };
        // return this.client.post('/create_event', type).then(res => res.data)
    }
}

export default ApiClient;
