import React from 'react'
import RootStore from "../src/store/mobx";
import {StoreContext} from './../src/context';
import ApiClient from "../src/store/ApiClient";

export const withStore = (WrappedComponent:any, apiClient:ApiClient, configureStore?: (store: RootStore) => any) => {
    const store = new RootStore(apiClient, undefined as any);

    if (configureStore) {
        configureStore(store)
    }

    return (props:any) => (
        <StoreContext.Provider value={store}>
                <WrappedComponent {...props}/>
        </StoreContext.Provider>
    )
};

export const wrapResponseInPromise = (response:any) => {
    return () => Promise.resolve(response);
};